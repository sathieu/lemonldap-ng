// Generated by CoffeeScript 1.10.0

/*
 * Session explorer
 */

(function() {
  var categories, hiddenAttributes, llapp, max, menu, overScheme, schemes;

  max = 25;

  schemes = {
    _whatToTrace: [
      function(t, v) {
        return "groupBy=substr(" + t + ",1)";
      }, function(t, v) {
        return t + "=" + v + "*&groupBy=" + t;
      }, function(t, v) {
        return t + "=" + v;
      }
    ],
    ipAddr: [
      function(t, v) {
        return "groupBy=net(" + t + ",16,1)";
      }, function(t, v) {
        if (!v.match(/:/)) {
          v = v + '.';
        }
        return t + "=" + v + "*&groupBy=net(" + t + ",32,2)";
      }, function(t, v) {
        if (!v.match(/:/)) {
          v = v + '.';
        }
        return t + "=" + v + "*&groupBy=net(" + t + ",48,3)";
      }, function(t, v) {
        if (!v.match(/:/)) {
          v = v + '.';
        }
        return t + "=" + v + "*&groupBy=net(" + t + ",128,4)";
      }, function(t, v) {
        return t + "=" + v + "&groupBy=_whatToTrace";
      }, function(t, v, q) {
        return q.replace(/\&groupBy.*$/, '') + ("&_whatToTrace=" + v);
      }
    ],
    _startTime: [
      function(t, v) {
        return "groupBy=substr(" + t + ",8)";
      }, function(t, v) {
        return t + "=" + v + "*&groupBy=substr(" + t + ",10)";
      }, function(t, v) {
        return t + "=" + v + "*&groupBy=substr(" + t + ",11)";
      }, function(t, v) {
        return t + "=" + v + "*&groupBy=substr(" + t + ",12)";
      }, function(t, v) {
        return t + "=" + v + "*&groupBy=_whatToTrace";
      }, function(t, v, q) {
        console.log(t);
        console.log(v);
        console.log(q);
        return q.replace(/\&groupBy.*$/, '') + ("&_whatToTrace=" + v);
      }
    ],
    doubleIp: [
      function(t, v) {
        return t;
      }, function(t, v) {
        return "_whatToTrace=" + v + "&groupBy=ipAddr";
      }, function(t, v, q) {
        return q.replace(/\&groupBy.*$/, '') + ("&ipAddr=" + v);
      }
    ]
  };

  overScheme = {
    _whatToTrace: function(t, v, level, over) {
      if (level === 1) {
        return t + "=" + v + "*&groupBy=substr(" + t + "," + (level + over + 1) + ")";
      } else {
        return null;
      }
    },
    ipAddr: function(t, v, level, over) {
      if (level > 0 && level < 4) {
        return t + "=" + v + "*&groupBy=net(" + t + "," + (16 * level + 4 * (over + 1)) + ",2)";
      } else {
        return null;
      }
    }
  };

  hiddenAttributes = '_password';

  categories = {
    dateTitle: ['_utime', '_startTime', '_updateTime', '_lastAuthnUTime', '_lastSeen'],
    connectionTitle: ['ipAddr', '_timezone', '_url'],
    authenticationTitle: ['_session_id', '_user', '_password', 'authenticationLevel'],
    modulesTitle: ['_auth', '_userDB', '_passwordDB', '_issuerDB', '_authChoice', '_authMulti', '_userDBMulti'],
    saml: ['_idp', '_idpConfKey', '_samlToken', '_lassoSessionDump', '_lassoIdentityDump'],
    groups: ['groups', 'hGroups'],
    ldap: ['dn'],
    BrowserID: ['_browserIdAnswer', '_browserIdAnswerRaw'],
    OpenIDConnect: ['_oidc_id_token', '_oidc_OP', '_oidc_access_token']
  };

  menu = {
    session: [
      {
        title: 'deleteSession',
        icon: 'trash'
      }
    ],
    home: []
  };


  /*
   * AngularJS application
   */

  llapp = angular.module('llngSessionsExplorer', ['ui.tree', 'ui.bootstrap', 'llApp']);

  llapp.controller('SessionsExplorerCtrl', [
    '$scope', '$translator', '$location', '$q', '$http', function($scope, $translator, $location, $q, $http) {
      var autoId, c, pathEvent, sessionType;
      $scope.links = links;
      $scope.menulinks = menulinks;
      $scope.staticPrefix = staticPrefix;
      $scope.scriptname = scriptname;
      $scope.formPrefix = formPrefix;
      $scope.availableLanguages = availableLanguages;
      $scope.waiting = true;
      $scope.showM = false;
      $scope.showT = true;
      $scope.data = [];
      $scope.currentScope = null;
      $scope.currentSession = null;
      $scope.menu = menu;
      $scope.translateP = $translator.translateP;
      $scope.translate = $translator.translate;
      $scope.translateTitle = function(node) {
        return $translator.translateField(node, 'title');
      };
      sessionType = 'global';
      $scope.menuClick = function(button) {
        if (button.popup) {
          window.open(button.popup);
        } else {
          if (!button.action) {
            button.action = button.title;
          }
          switch (typeof button.action) {
            case 'function':
              button.action($scope.currentNode, $scope);
              break;
            case 'string':
              $scope[button.action]();
              break;
            default:
              console.log(typeof button.action);
          }
        }
        return $scope.showM = false;
      };
      $scope.deleteSession = function() {
        $scope.waiting = true;
        return $http['delete'](scriptname + "sessions/" + sessionType + "/" + $scope.currentSession.id).then(function(response) {
          $scope.currentSession = null;
          $scope.currentScope.remove();
          return $scope.waiting = false;
        }, function(resp) {
          $scope.currentSession = null;
          $scope.currentScope.remove();
          return $scope.waiting = false;
        });
      };
      $scope.stoggle = function(scope) {
        var node;
        node = scope.$modelValue;
        if (node.nodes.length === 0) {
          $scope.updateTree(node.value, node.nodes, node.level, node.over, node.query, node.count);
        }
        return scope.toggle();
      };
      $scope.displaySession = function(scope) {
        var sessionId, transformSession;
        transformSession = function(session) {
          var _insert, _stToStr, attr, attrs, category, i, id, j, k, key, l, len, len1, len2, ref, ref1, res, subres, time, tmp, value;
          _stToStr = function(s) {
            return s;
          };
          _insert = function(re, title) {
            var key, reg, tmp, value;
            tmp = [];
            reg = new RegExp(re);
            for (key in session) {
              value = session[key];
              if (key.match(reg) && value) {
                tmp.push({
                  title: key,
                  value: value
                });
                delete session[key];
              }
            }
            if (tmp.length > 0) {
              return res.push({
                title: title,
                nodes: tmp
              });
            }
          };
          time = session._utime;
          id = session._session_id;
          for (key in session) {
            value = session[key];
            if (!value) {
              delete session[key];
            } else {
              if (typeof session === 'string' && value.match(/; /)) {
                session[key] = value.split('; ');
              }
              if (typeof session[key] !== 'object') {
                if (hiddenAttributes.match(new RegExp('\b' + key + '\b'))) {
                  session[key] = '********';
                } else if (key.match(/^(_utime|_lastAuthnUTime|_lastSeen|notification)$/)) {
                  session[key] = $scope.localeDate(value);
                } else if (key.match(/^(_startTime|_updateTime)$/)) {
                  session[key] = _stToStr(value);
                }
              }
            }
          }
          res = [];
          for (category in categories) {
            attrs = categories[category];
            subres = [];
            for (i = 0, len = attrs.length; i < len; i++) {
              attr = attrs[i];
              if (session[attr]) {
                subres.push({
                  title: attr,
                  value: session[attr]
                });
                delete session[attr];
              }
            }
            if (subres.length > 0) {
              res.push({
                title: "__" + category + "__",
                nodes: subres
              });
            }
          }
          _insert('^openid', 'OpenID');
          _insert('^notification_(.+)', '__notificationsDone__');
          if (session._loginHistory) {
            tmp = [];
            if (session._loginHistory.successLogin) {
              ref = session._loginHistory.successLogin;
              for (j = 0, len1 = ref.length; j < len1; j++) {
                l = ref[j];
                tmp.push({
                  t: l._utime,
                  title: $scope.localeDate(l._utime),
                  value: "Success (IP " + l.ipAddr + ")"
                });
              }
            }
            if (session._loginHistory.failedLogin) {
              ref1 = session._loginHistory.failedLogin;
              for (k = 0, len2 = ref1.length; k < len2; k++) {
                l = ref1[k];
                tmp.push({
                  t: l._utime,
                  title: $scope.localeDate(l._utime),
                  value: l.error + " (IP " + l.ipAddr + ")"
                });
              }
            }
            delete session._loginHistory;
            tmp.sort(function(a, b) {
              return a.t - b.t;
            });
            res.push({
              title: '__loginHistory__',
              nodes: tmp
            });
          }
          tmp = [];
          for (key in session) {
            value = session[key];
            tmp.push({
              title: key,
              value: value
            });
          }
          tmp.sort(function(a, b) {
            if (a.title > b.title) {
              return 1;
            } else if (a.title < b.title) {
              return -1;
            } else {
              return 0;
            }
          });
          res.push({
            title: '__attributesAndMacros__',
            nodes: tmp
          });
          return {
            _utime: time,
            id: id,
            nodes: res
          };
        };
        $scope.currentScope = scope;
        sessionId = scope.$modelValue.session;
        $http.get(scriptname + "sessions/" + sessionType + "/" + sessionId).then(function(response) {
          return $scope.currentSession = transformSession(response.data);
        });
        return $scope.showT = false;
      };
      $scope.localeDate = function(s) {
        var d;
        d = new Date(s * 1000);
        return d.toLocaleString();
      };
      $scope.getLanguage = function(lang) {
        $scope.lang = lang;
        $scope.form = 'white';
        $scope.init();
        return $scope.showM = false;
      };
      pathEvent = function(event, next, current) {
        var n;
        n = next.match(/#\/(\w+)/);
        sessionType = 'global';
        if (n === null) {
          $scope.type = '_whatToTrace';
        } else if (n[1].match(/^(persistent)$/)) {
          sessionType = RegExp.$1;
          $scope.type = '_session_uid';
        } else {
          $scope.type = n[1];
        }
        return $scope.init();
      };
      $scope.$on('$locationChangeSuccess', pathEvent);
      autoId = 0;
      $scope.updateTree = function(value, node, level, over, currentQuery, count) {
        var query, scheme, tmp;
        $scope.waiting = true;
        scheme = schemes[$scope.type] ? schemes[$scope.type] : $scope.type === '_updateTime' ? schemes._startTime : schemes._whatToTrace;
        query = scheme[level]($scope.type, value, currentQuery);
        if (count > max && overScheme[$scope.type]) {
          if (tmp = overScheme[$scope.type]($scope.type, value, level, over, currentQuery)) {
            over++;
            query = tmp;
            level = level - 1;
          } else {
            over = 0;
          }
        } else {
          over = 0;
        }
        return $http.get(scriptname + "sessions/" + sessionType + "?" + query).then(function(response) {
          var data, i, len, n, ref;
          data = response.data;
          if (data.result) {
            ref = data.values;
            for (i = 0, len = ref.length; i < len; i++) {
              n = ref[i];
              autoId++;
              n.id = "node" + autoId;
              if (level < scheme.length - 1) {
                n.nodes = [];
                n.level = level + 1;
                n.query = query;
                n.over = over;
                if ($scope.type.match(/^(?:start|update)Time$/)) {
                  n.title = n.value.replace(/^(\d{8})(\d{2})(\d{2})$/, '$2:$3').replace(/^(\d{8})(\d{2})(\d)$/, '$2:$30').replace(/^(\d{8})(\d{2})$/, '$2h').replace(/^(\d{4})(\d{2})(\d{2})/, '$1-$2-$3');
                }
              }
              node.push(n);
            }
            if (value === '') {
              $scope.total = data.total;
            }
          }
          return $scope.waiting = false;
        }, function(resp) {
          return $scope.waiting = false;
        });
      };
      $scope.init = function() {
        $scope.waiting = true;
        $scope.data = [];
        return $q.all([$translator.init($scope.lang), $scope.updateTree('', $scope.data, 0, 0)]).then(function() {
          return $scope.waiting = false;
        }, function(resp) {
          return $scope.waiting = false;
        });
      };
      c = $location.path().match(/^\/(\w+)/);
      return $scope.type = c ? c[1] : '_whatToTrace';
    }
  ]);

}).call(this);
