#!/usr/bin/perl

use strict;
use Getopt::Long;
use Lemonldap::NG::Common::Conf;

my %opts;
my $result = GetOptions( \%opts, 'help|h', 'current|c=s', 'new|n=s', 'latest|l',
    'force|f' );

if ( $opts{help} or not( $opts{current} and $opts{new} ) ) {
    print STDERR "
               ## Lemonldap::NG configuration converter ##

Usage: $0 --current=/current/lemonldap-ng.ini --new=/new/lemonldap-ng.ini

other parameters:
    --latest -l
        convert only last configuration
    --force -f
        continue even if an error occurs

";
    exit 1;
}

foreach ( $opts{current}, $opts{new} ) {
    unless ( -e $_ ) {
        print STDERR "$_ does not exist\n";
        exit 2;
    }
    unless ( -r $_ ) {
        print STDERR "$_ is not readable\n";
        exit 3;
    }
}

my $old = Lemonldap::NG::Common::Conf->new(
    {
        confFile => $opts{current},
    }
);
unless ($old) {
    print STDERR
      "Failed to get current conf : $Lemonldap::NG::Common::Conf::msg\n";
    exit 4;
}

my %newargs = (
    force       => 1,
    noCache     => 1,
    cfgNumFixed => 1,
);

my $new = Lemonldap::NG::Common::Conf->new(
    {
        confFile => $opts{new},
    }
);
unless ($new) {
    print STDERR
      "Failed to create new conf object : $Lemonldap::NG::Common::Conf::msg\n";
    exit 5;
}

my @available;
if ( $opts{latest} ) {
    @available = $old->lastCfg();
}
else {
    @available = $old->available();
}

foreach (@available) {
    my $conf = $old->getConf( { cfgNum => $_, noCache => 1 } );
    unless ($conf) {
        print STDERR
          "\nFailed to get conf $_ : $Lemonldap::NG::Common::Conf::msg\n";
        next if ( $opts{force} );
        exit 6;
    }
    if ( my $r = $new->saveConf( $conf, %newargs ) ) {
        print "Conf $conf->{cfgNum} stored\n";
        next;
    }
    print STDERR
"Unable to store configuration $conf->{cfgNum}: $Lemonldap::NG::Common::Conf::msg";
    next if ( $opts{force} );
    exit 7;
}

__END__

=head1 NAME

=encoding utf8

convertConfig - tool used to change Lemonldap::NG configuration database.

=head1 SYNOPSIS

  convertConfig --current=/current/lemonldap-ng.ini --new=/new/lemonldap-ng.ini

  # Convert only latest (loose history)
  convertConfig  --latest --current=... --new=...

  # Continue even if an error occurs
  convertConfig  --force -f --current=... --new=...


=head1 DESCRIPTION

convertConfig is a command line toot that can be used to do initialize a new
Lemonldap::NG configuration database keeping current configuration and history.

=head1 SEE ALSO

L<http://lemonldap-ng.org/>

=head1 AUTHORS

=over

=item Clement Oudot, E<lt>clem.oudot@gmail.comE<gt>

=item Xavier Guimard, E<lt>x.guimard@free.frE<gt>

=back

=head1 BUG REPORT

Use OW2 system to report bug or ask for features:
L<https://gitlab.ow2.org/lemonldap-ng/lemonldap-ng/issues>

=head1 DOWNLOAD

Lemonldap::NG is available at
L<http://forge.objectweb.org/project/showfiles.php?group_id=274>

=head1 COPYRIGHT AND LICENSE

=over

=item Copyright (C) 2008-2016 by Xavier Guimard, E<lt>x.guimard@free.frE<gt>

=item Copyright (C) 2008-2016 by Clément Oudot, E<lt>clem.oudot@gmail.comE<gt>

=back

This library is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2, or (at your option)
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see L<http://www.gnu.org/licenses/>.

=cut
